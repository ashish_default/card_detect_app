package com.example.gorummy.floatingviews;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.gorummy.MainActivity;
import com.example.gorummy.ScreenTranslatorService;
import com.example.gorummy.utils.HomeWatcher;
import com.example.gorummy.utils.PermissionUtil;
import com.example.gorummy.utils.SettingUtil;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.WINDOW_SERVICE;

public abstract class FloatingView {

    private static final String TAG = FloatingView.class.getSimpleName();

    private Context context;
    private CustomLayout rootView;
    private OnBackButtonPressedListener onBackButtonPressedListener;
    private WindowManager windowManager;
    private WindowManager.LayoutParams floatingLayoutParams;
    private boolean isAttached = false;
    private List<AsyncTask> manageTask = new ArrayList<>();
    private final static List<FloatingView> nonPrimaryViews = new ArrayList<>();
    private HomeWatcher homeWatcher;

    public FloatingView(Context context){
        this.context = context;
        rootView = new CustomLayout(context);
        View innerView = LayoutInflater.from(context).inflate(getLayoutId(), null);
        rootView.addView(innerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);

        int type;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            type = WindowManager.LayoutParams.TYPE_PHONE;
        }

        int flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
        if(!layoutFocusable()){
            flags = flags | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        }

        if(canMoveOutside()){
            flags = flags | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
        }

        floatingLayoutParams = new WindowManager.LayoutParams(
                getLayoutSize(), getLayoutSize(), type, flags, PixelFormat.TRANSLUCENT);
        if (fullScreenMode()) {
            floatingLayoutParams.flags = floatingLayoutParams.flags | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
        }

        floatingLayoutParams.gravity = getLayoutGravity();

        if(isPrimaryView()){
            Integer[] lastPosition = SettingUtil.INSTANCE.getLastMainBarPosition();
            if(lastPosition[0] != null && lastPosition[0] != -1) {
                floatingLayoutParams.x = lastPosition[0];
            }
            if(lastPosition[1] != null && lastPosition[1] != -1){
                floatingLayoutParams.y = lastPosition[1];
            }
        }


    }

    public void onViewStart(){}
    protected abstract int getLayoutId();
    protected boolean layoutFocusable() {
        return false;
    }
    protected boolean canMoveOutside(){
        return false;
    }

    protected int getLayoutGravity(){
        return Gravity.TOP | Gravity.LEFT;
    }

    protected boolean isPrimaryView(){
        return false;
    }

    public boolean isAttached(){ return isAttached;}

    public View getRootView() {
        return rootView;
    }

    public WindowManager getWindowManager() {
        return windowManager;
    }

    public WindowManager.LayoutParams getFloatingLayoutParams() {
        return floatingLayoutParams;
    }

    public void setOnBackButtonPressedListener(OnBackButtonPressedListener onBackButtonPressedListener) {
        this.onBackButtonPressedListener = onBackButtonPressedListener;
    }

    public boolean onBackButtonPressed(){
        if(onBackButtonPressedListener != null){
            return onBackButtonPressedListener.onBackButtonPressed(this);
        }
        return false;
    }

    protected boolean fullScreenMode() {
        return false;
    }

    public Context getContext() {
        return context;
    }

    protected int getLayoutSize() {
        return WindowManager.LayoutParams.WRAP_CONTENT;
    }

    public void attachToWindow() {
        if (!isAttached) {
            if (PermissionUtil.canDrawOverlays(context)) {
                windowManager.addView(rootView, floatingLayoutParams);
                isAttached = true;
                if (!isPrimaryView()) {
                    synchronized (nonPrimaryViews) {
                        nonPrimaryViews.add(this);
                    }
                }
            } else {
                MainActivity.start(context);
                ScreenTranslatorService.stop(true);
            }
        }
    }

    public void detachFromWindow(){
        removeHomeButtonWatcher();

        if(isAttached){
            for(AsyncTask asyncTask : manageTask) {
                if(asyncTask != null && !asyncTask.isCancelled()) {
                    asyncTask.cancel(true);
                }
            }

            manageTask.clear();

            if(isPrimaryView()) {
                SettingUtil.INSTANCE.setLastMainBarPosition(
                        new Integer[]{floatingLayoutParams.x, floatingLayoutParams.y});
            }

            windowManager.removeView(rootView);
            isAttached = false;

            if(!isPrimaryView()) {
                synchronized (nonPrimaryViews) {
                    nonPrimaryViews.remove(this);
                }
            }

        }

    }

    public void removeHomeButtonWatcher() {
        new Handler().post(new Runnable() {
           @Override
           public void run() {
               try{
                   if(homeWatcher != null) {
                       homeWatcher.setOnHomePressedListener(null);
                       homeWatcher.stopWatch();
                       homeWatcher = null;
                   }
               } catch(Throwable t) {
                   t.printStackTrace();
               }
           }
        });
    }

    void updateViewLayout() {
        try{
            getWindowManager().updateViewLayout(getRootView(), getFloatingLayoutParams());
        } catch(IllegalArgumentException e){
            Log.i(TAG, e.getMessage());
        }
    }

    private class CustomLayout extends LinearLayout {

        public CustomLayout(Context context){
            super(context);
        }

        public CustomLayout(Context context, @Nullable AttributeSet attrs){
            super(context, attrs);
        }

        public CustomLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr){
            super(context, attrs, defStyleAttr);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public CustomLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
            super(context, attrs, defStyleAttr, defStyleRes);
        }

        @Override
        protected void onAttachedToWindow(){
            super.onAttachedToWindow();
            FloatingView.this.onViewStart();
        }

        @Override
        public boolean dispatchKeyEvent(KeyEvent event){
            if(event.getKeyCode() == KeyEvent.KEYCODE_BACK && getKeyDispatcherState() != null){
                if(event.getAction() == KeyEvent.ACTION_DOWN && event.getRepeatCount() ==0) {
                    getKeyDispatcherState().startTracking(event, this);
                    return true;
                } else if(event.getAction() == KeyEvent.ACTION_UP) {
                    getKeyDispatcherState().handleUpEvent(event);
                    if(event.isTracking() && !event.isCanceled()) {
                        if(onBackButtonPressed()){
                            return true;
                        }
                    }
                }
            }

            return super.dispatchKeyEvent(event);
        }
    }

    public interface OnBackButtonPressedListener {
        boolean onBackButtonPressed(FloatingView floatingView);
    }

}
