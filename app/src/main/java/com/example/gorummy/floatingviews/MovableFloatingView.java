package com.example.gorummy.floatingviews;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.OvershootInterpolator;

import androidx.annotation.RequiresApi;

import com.example.gorummy.utils.UIUtil;

public abstract class MovableFloatingView extends FloatingView {

    private static final String TAG = MovableFloatingView.class.getSimpleName();

    private static final float MOVE_TO_EDGE_OVERSHOOT_TENSION = 1.25f;
    private static final int MOVE_TO_EDGE_MARGIN_DP = -20;
    private static final long MOVE_TO_EDGE_DURATION = 450L;
    private static final float DESTINATION_ALPHA = 0.2f;

    private final TimeInterpolator mMoveEdgeInterpolator;
    private View dragView;
    private float fromAlpha = 1.0f;
    private ValueAnimator mFadeOutAnimator;
    private TouchInterceptor touchInterceptor;
    private ValueAnimator mMoveEdgeAnimator;

    private static final long FADE_OUT_ANIM_DURATION = 800L;
    private static final long FADE_OUT_ANIM_DELAY = 1000L;

    public MovableFloatingView(Context context) {
        super(context);
        mMoveEdgeInterpolator = new OvershootInterpolator(MOVE_TO_EDGE_OVERSHOOT_TENSION);
    }

    protected boolean enableTransparentWhenMoved() {
        return false;
    }

    public void setTouchInterceptor(TouchInterceptor touchInterceptor){
        this.touchInterceptor = touchInterceptor;
    }

    protected boolean enableAutoMoveToEdge() {
        return false;
    }

    @Override
    public void attachToWindow() {
        super.attachToWindow();
        moveToEdgeOrFadeOut();
    }


    protected void rescheduleFadeOut() {
        if (mFadeOutAnimator != null) {
            mFadeOutAnimator.cancel();
        }
        getRootView().setAlpha(fromAlpha);
        if (enableTransparentWhenMoved()) {
            fadeOut();
        }
    }

    private boolean moveToEdgeOrFadeOut(){
        if(enableAutoMoveToEdge()){
            moveToEdge();
            return true;
        } else if(enableTransparentWhenMoved()){
            fadeOut();
            return true;
        }

        return false;
    }

    private void moveToEdge() {
        int[] edgePosition = getEdgePosition(getFloatingLayoutParams().x, getFloatingLayoutParams().y);

        moveTo(getFloatingLayoutParams().x, getFloatingLayoutParams().y,
                edgePosition[0], edgePosition[1], true);
    }

    private int[] getEdgePosition(int currentX, int currentY){
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);

        int viewWidth = getRootView().getWidth();
        int viewCenterX = currentX + viewWidth / 2;

        int margin = (int) UIUtil.INSTANCE.dpToPx(getContext(), MOVE_TO_EDGE_MARGIN_DP);
        int edgeX;
        if(viewCenterX < metrics.widthPixels / 2) {
            edgeX = margin;
        } else {
            edgeX = metrics.widthPixels - viewWidth - margin;
        }

        return new int[]{edgeX, currentY};
    }

    private void fadeOut() {
        if(mFadeOutAnimator != null) {
            mFadeOutAnimator.cancel();
        }

        mFadeOutAnimator = ValueAnimator.ofFloat(fromAlpha, DESTINATION_ALPHA);
        mFadeOutAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                getRootView().setAlpha((Float) animation.getAnimatedValue());
            }
        });

        mFadeOutAnimator.setDuration(FADE_OUT_ANIM_DURATION);
        mFadeOutAnimator.setStartDelay(FADE_OUT_ANIM_DELAY);
        mFadeOutAnimator.start();
    }

    private void moveTo(int currentX, int currentY, int goalPositionX, int goalPositionY, boolean withAnimation) {
        if(withAnimation){
            if(goalPositionX == currentX) {
                mMoveEdgeAnimator = ValueAnimator.ofInt(currentY, goalPositionY);
                mMoveEdgeAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        getFloatingLayoutParams().y = (Integer) animation.getAnimatedValue();
                        updateViewLayout();
                    }
                });
            } else {
                getFloatingLayoutParams().y = goalPositionY;
                mMoveEdgeAnimator = ValueAnimator.ofInt(currentX, goalPositionX);
                mMoveEdgeAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        getFloatingLayoutParams().x = (Integer) animation.getAnimatedValue();
                        updateViewLayout();
                    }
                });
            }

            mMoveEdgeAnimator.setDuration(MOVE_TO_EDGE_DURATION);
            mMoveEdgeAnimator.setInterpolator(mMoveEdgeInterpolator);
            mMoveEdgeAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    if(enableTransparentWhenMoved()){
                        fadeOut();
                    }
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            mMoveEdgeAnimator.start();

        } else {
            if(getFloatingLayoutParams().x != goalPositionX || getFloatingLayoutParams().y != goalPositionY){
                getFloatingLayoutParams().x = goalPositionX;
                getFloatingLayoutParams().y = goalPositionY;
                updateViewLayout();
            }
        }
    }

    protected void setDragView(View view){
        this.dragView = view;
        this.fromAlpha = view.getAlpha();
        view.setClickable(true);
        view.setFocusable(true);
        view.setOnTouchListener(onTouchListener);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private boolean isAlignParentLeft() {
        return (Gravity.getAbsoluteGravity(getLayoutGravity(), getRootView().getLayoutDirection()) & Gravity.HORIZONTAL_GRAVITY_MASK) == Gravity.LEFT;
    }

    private boolean isAlignParentTop() {
        return (getLayoutGravity() & Gravity.VERTICAL_GRAVITY_MASK) == Gravity.TOP;
    }

    private View.OnTouchListener onTouchListener = new View.OnTouchListener(){
      private int initX, initY;
      private float initTouchX, initTouchY;
      private boolean hasMoved = false;

      @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
      @Override
      public boolean onTouch(View v, MotionEvent event){
          if(mFadeOutAnimator != null && mFadeOutAnimator.isRunning()) {
              mFadeOutAnimator.cancel();
          }

          if(enableTransparentWhenMoved()){
              getRootView().setAlpha(fromAlpha);
          }

          if(touchInterceptor != null && touchInterceptor.onTouch(v, event, hasMoved)){
              return true;
          }

          switch(event.getAction()){
              case MotionEvent.ACTION_DOWN:
                  hasMoved = false;
                  initX = getFloatingLayoutParams().x;
                  initY = getFloatingLayoutParams().y;
                  initTouchX = event.getRawX();
                  initTouchY = event.getRawY();
                  break;
              case MotionEvent.ACTION_UP:
                  boolean temp = hasMoved;
                  hasMoved = false;
                  moveToEdgeOrFadeOut();
                  if(temp) {
                      return true;
                  }
                  break;
              case MotionEvent.ACTION_MOVE:
                  if (Math.abs(initTouchX - event.getRawX()) > 20 || Math.abs(initTouchY - event.getRawY()) > 20) {
                      hasMoved = true;
                  }
                  int xDiff = (int) (event.getRawX() - initTouchX);
                  int yDiff = (int) (event.getRawY() - initTouchY);

                  int nextX = initX + (isAlignParentLeft() ? xDiff: -xDiff);
                  int nextY = initY + (isAlignParentTop() ? yDiff: -yDiff);

                  if(nextX < 0){
                      nextX = 0;
                  }
                  if(nextY < 0) {
                      nextY = 0;
                  }

                  getFloatingLayoutParams().x = nextX;
                  getFloatingLayoutParams().y = nextY;
                  updateViewLayout();
                  break;
          }

          return false;
      }


    };
}

interface TouchInterceptor {
    boolean onTouch(View v, MotionEvent event, boolean hasMoved);
}
