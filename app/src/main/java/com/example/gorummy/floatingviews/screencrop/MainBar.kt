package com.example.gorummy.floatingviews.screencrop

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.content.Context.ACTIVITY_SERVICE
import android.graphics.Rect
import android.util.Log
import android.view.Gravity
import android.view.View
import com.example.gorummy.*
import com.example.gorummy.events.EventUtil
import com.example.gorummy.floatingviews.MenuView
import com.example.gorummy.floatingviews.MovableFloatingView
import com.example.gorummy.screenshot.events.CardDetectResultEvent
import com.example.gorummy.state.InitState
import com.example.gorummy.utils.SettingUtil
import com.example.gorummy.utils.setVisible
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class MainBar(context: Context) : MovableFloatingView(context), RealButtonHandler {

    override fun isPrimaryView(): Boolean {
        return true;
    }

    override fun getLayoutId(): Int = R.layout.view_floating_bar_new
    private val viewMenu: View = rootView.findViewById(R.id.view_menu);
    private val btSelectArea: View = rootView.findViewById(R.id.bt_selectArea)
    private val TAG:String = "MainBar"
    private var cardResultView: CardResultView? = null
    private val progressBar: View = rootView.findViewById(R.id.pg_progress)
    private val menuView: MenuView by lazy {
        MenuView(context, listOf(R.string.menu_exit), onMenuItemClickedListener)
    }

    init {
        setViews()
        setDragView(viewMenu)
    }

    private fun setViews() {
        viewMenu.setOnClickListener {
            rescheduleFadeOut()

            val location = IntArray(2)
            viewMenu.getLocationOnScreen(location)

            val rect = Rect()
            rect.left = location[0]
            rect.top = location[1]
            rect.bottom = rect.top
            rect.right = rect.left

            Log.i(TAG, "Menu view rect: $rect")
            menuView.attachToWindow(rect)
        }
    }

    override fun enableTransparentWhenMoved(): Boolean =
            StateManager.state is InitState

    override fun onHomeButtonPressed() {
        TODO("Not yet implemented")
    }

    override fun attachToWindow() {
        super.attachToWindow();
        SettingUtil.isAppShowing = true

        btSelectArea.setOnClickListener {
            StateManager.startSelection()
        }

        EventUtil.register(this)
    }

    private fun showView(show: Boolean) = rootView.setVisible(show)

    @SuppressLint("RtlHardcoded")
    override fun getLayoutGravity(): Int = Gravity.TOP or Gravity.RIGHT

    init {
        StateManager.listener = object : OnStateChangedListener {
            override fun onStateChanged(state: StateName){
                Log.i(TAG, "on state changed called")
                showView(true)

            }

            override fun beforeScreenshot() {
                Log.i(TAG, "before screenshot called")
//                Log.i(TAG,getLollipop())
//                TODO("Not yet implemented")
            }

            override fun screenshotSuccess() {
                Log.i(TAG, "screen shot success called")
                progressBar.setVisible(true)
                attachToWindow()
            }

            override fun screenshotFailed(errorCode: Int, e: Throwable?) {
                Log.i(TAG, "screenshot failed called")
                attachToWindow()
//                resetAll()
//                rescheduleFadeOut()
//
//                val msg = when (errorCode) {
//                    ScreenshotHandler.ERROR_CODE_TIMEOUT ->
//                        R.string.dialog_content_screenshotTimeout.asString()
//                    ScreenshotHandler.ERROR_CODE_IMAGE_FORMAT_ERROR ->
//                        R.string.dialog_content_screenshotWithImageFormatError
//                                .asFormatString(e?.message ?: "Unknown")
//                    ScreenshotHandler.ERROR_CODE_OUT_OF_MEMORY ->
//                        R.string.error_screenshot_out_of_memory.asString()
//                    else ->
//                        R.string.dialog_content_screenshotWithUnknownError
//                                .asFormatString(e?.message ?: "Unknown")
//                }

//                showErrorDialog(DialogView.Type.CONFIRM_ONLY,
//                        getContext().getString(R.string.dialog_title_error), msg)
            }

            override fun drawResultWindow() {
                cardResultView = CardResultView(getContext()).apply {
                    this.attachToWindow(StateManager.result)
                }

                attachToWindow()
            }
        }
    }

    private val onMenuItemClickedListener = object: MenuView.OnMenuItemClickListener {
        override fun onMenuItemClicked(position: Int, item: Int) {
            when(item) {
                R.string.menu_exit -> ScreenTranslatorService.stop(true)
            }
        }
    }


//    fun showErrorDialog(type: DialogView.Type, title: String, msg: String, callback: DialogView.OnDialogViewCallback? = null) {
//        dialogView.reset()
//        dialogView.setType(type)
//        dialogView.setTitle(title)
//        dialogView.setContentMsg(msg)
//        dialogView.attachToWindow()
//        dialogView.setCallback(callback)
//    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onCardDetectResultEvent(event: CardDetectResultEvent){
        Log.i(TAG, event.toString())
        progressBar.setVisible(false)
    }

    private fun activityManager(): ActivityManager? {
        return context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    }

}