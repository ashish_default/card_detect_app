package com.example.gorummy.floatingviews.screencrop

interface RealButtonHandler {
    fun onBackButtonPressed(): Boolean
    fun onHomeButtonPressed()
}