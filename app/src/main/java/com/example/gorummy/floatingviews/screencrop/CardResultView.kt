package com.example.gorummy.floatingviews.screencrop

import android.content.Context
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.TextView
import com.example.gorummy.R
import com.example.gorummy.StateManager
import com.example.gorummy.events.EventUtil
import com.example.gorummy.floatingviews.FloatingView
import com.example.gorummy.floatingviews.MovableFloatingView
import com.example.gorummy.screenshot.events.ScreenshotTakeEvent
import com.example.gorummy.state.ClearingState
import com.example.gorummy.state.ScreenshotTakeState
import com.example.gorummy.utils.UIUtil
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONArray
import org.json.JSONObject

class CardResultView(context: Context): MovableFloatingView(context) {
    private val TAG:String = javaClass.simpleName

    private val resultView: View = rootView.findViewById(R.id.card_result_view)
    private val closeResultBtn: View = rootView.findViewById(R.id.btn_close_result);
//    private val resultText: TextView = rootView.findViewById(R.id.card_result)
    private val bottomCardResultView: TextView = rootView.findViewById(R.id.bottom_card_result)
    private val openCardResultView:TextView = rootView.findViewById(R.id.open_card_result)
    private val closedCardResultView: TextView = rootView.findViewById(R.id.closed_card_result)
    override fun getLayoutId(): Int = R.layout.card_detect_result


//    override fun getLayoutSize(): Int = WindowManager.LayoutParams.MATCH_PARENT

    init {
        setDragView(resultView)
    }

    fun attachToWindow(result:JSONObject?) {

        var screensize = UIUtil.getScreenSize()
        var viewLayoutParams = resultView.layoutParams as LinearLayout.LayoutParams
        viewLayoutParams.leftMargin = screensize[0]/4
//        viewLayoutParams.y = (screensize[1]/2).toFloat()
        resultView.layoutParams = viewLayoutParams
        Log.i(TAG, result.toString())
//        resultText.text = result?.toString()
        setCardTextView(result)
        this.attachToWindow()
    }

    private fun setCardTextView(result: JSONObject?){
        var bottomCardString:String = getCardString(result?.getJSONObject("bottom_cards")!!.getJSONArray("cards"),
                result!!.getJSONObject("bottom_cards").getInt("jocker_count"))
        var openCardString: String = getCardString(result?.getJSONObject("open_cards")!!.getJSONArray("cards"),
                result!!.getJSONObject("open_cards").getInt("jocker_count"))
        var closeCardString: String = getCardString(result?.getJSONObject("close_cards")!!.getJSONArray("cards"),
                result!!.getJSONObject("close_cards").getInt("jocker_count"))

        bottomCardResultView.text = bottomCardString
        openCardResultView.text = openCardString
        closedCardResultView.text = closeCardString

    }

    override fun attachToWindow() {
        super.attachToWindow()
        closeResultBtn.setOnClickListener {
            StateManager.enterState(ClearingState)
            this.detachFromWindow()
        }

        EventUtil.register(this)
    }


    private fun getCardString(cards: JSONArray, jokerCount: Int):String {
        var cardString: String = ""

        for (i in 0 until cards.length()){
            var card = cards.getJSONArray(i)
            var cardNumber:String = card[0] as String
            var cardType:String = card[1] as String
            cardString += cardNumber.toUpperCase() + "-" + cardType.toUpperCase().toCharArray()[0] + ", "

            // add new line to have better view in bottom card text view
            if((i+1)%3 == 0){
                cardString += "\n"
            }
        }

        cardString += "JokerCount-$jokerCount"
        Log.i(TAG, cardString)
        return cardString
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onScreenshotTakeEvent(event: ScreenshotTakeEvent){
        Log.i(TAG, event.toString())
        this.detachFromWindow()
    }


}