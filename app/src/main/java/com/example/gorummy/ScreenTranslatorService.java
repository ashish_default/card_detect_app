package com.example.gorummy;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.gorummy.floatingviews.FloatingView;
import com.example.gorummy.floatingviews.screencrop.MainBar;
import com.example.gorummy.utils.SettingUtil;
import com.example.gorummy.utils.Utils;

public class ScreenTranslatorService extends Service {

    private static final String TAG = ScreenTranslatorService.class.getSimpleName();
    private static ScreenTranslatorService _instance;

    private static FloatingView mainFloatingView;
    private SettingUtil spUtil;


    public ScreenTranslatorService() { _instance = this; }

    public static Context getContext() {
        if(_instance != null) {
            return _instance;
        }

        return null;
    }

    public static void start(Context context, boolean showFloatingView) {
        if(!isRunning()){
            context.startService(new Intent(context, ScreenTranslatorService.class));
        } else if (_instance != null) {
            if(showFloatingView) {
                _instance._startFloatingView();
            } else {
                _instance._stopFloatingView(true);
            }
        }

    }

    public static void stop(boolean dismissNotify) {
        if (_instance != null) {
//            _instance.dismissNotify = dismissNotify;
            _instance._stopFloatingView(false);
            _instance.stopSelf();
        }
    }

    private void _stopFloatingView(boolean updateNotify) {
        if(mainFloatingView != null){
            mainFloatingView.detachFromWindow();
        }

        if(updateNotify) {
         //   updateNotification();
            Log.i(TAG, "update notify");
        }

        Log.i(TAG, "show state changed");
    }

    private static boolean isRunning() {
        return Utils.isServiceRunning(ScreenTranslatorService.class) && _instance != null;
    }

    private boolean _isFloatingViewShowing() {
        return mainFloatingView != null && mainFloatingView.isAttached();
    }

    private void _startFloatingView() {
        if(_isFloatingViewShowing()){
            mainFloatingView.detachFromWindow();
        }

        mainFloatingView = new MainBar(this);
        mainFloatingView.attachToWindow();

    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if (_instance == null) {
            _instance = this;
        }

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if(_instance == null) {
            _instance = this;
        }

        spUtil = SettingUtil.INSTANCE;

        if (SettingUtil.INSTANCE.isAppShowing()) {
            _startFloatingView();
        }

//        startForeground();

    }

    private void startForeground() {
    }

}
