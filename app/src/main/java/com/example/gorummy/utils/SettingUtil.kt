package com.example.gorummy.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.example.gorummy.CoreApplication

object SettingUtil {

    private const val KEY_APP_SHOWING = "KEY_APP_SHOWING";
    private const val KEY_LAST_MAIN_BAR_POSITION_X = "KEY_LAST_MAIN_BAR_POSITION_X";
    private const val KEY_LAST_MAIN_BAR_POSITION_Y = "KEY_LAST_MAIN_BAR_POSITION_Y";

    private val context: Context
        get() {
            return CoreApplication.instance
        }

    private val sp: SharedPreferences
            by lazy { PreferenceManager.getDefaultSharedPreferences(context) }

    var isAppShowing: Boolean
        get() {
            return sp.getBoolean(KEY_APP_SHOWING, true)
        }
        set(value) {
            sp.edit().putBoolean(KEY_APP_SHOWING, value).apply()
        }

    var lastMainBarPosition: Array<Int>
        get() = arrayOf(
                sp.getInt(KEY_LAST_MAIN_BAR_POSITION_X, -1),
                sp.getInt(KEY_LAST_MAIN_BAR_POSITION_Y, -1))
        set(value) {
            sp.edit().putInt(KEY_LAST_MAIN_BAR_POSITION_X, value[0])
                    .putInt(KEY_LAST_MAIN_BAR_POSITION_Y, value[1]).apply()
        }


}