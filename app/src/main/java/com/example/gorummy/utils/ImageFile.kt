package com.example.gorummy.utils

import java.io.File


data class ImageFile(val file: File, val width: Int, val height: Int)