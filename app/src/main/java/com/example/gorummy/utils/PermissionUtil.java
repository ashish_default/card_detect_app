package com.example.gorummy.utils;

import android.app.AppOpsManager;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

public class PermissionUtil {

    private static final String TAG = PermissionUtil.class.getSimpleName();

    public static boolean canDrawOverlays(Context context) {
        Log.i(TAG, "check draw overlay permissions");
        // check not required for android version less than Version M
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        } else if(Build.VERSION.SDK_INT > Build.VERSION_CODES.O_MR1) {
            AppOpsManager appOpsMgr = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            int mode = appOpsMgr.checkOpNoThrow("android:system_alert_window", android.os.Process.myUid(), context.getPackageName());
            Log.d(TAG, "android:system_alert_window: mode=" + mode);
            return Settings.canDrawOverlays(context);
        } else {
            if(Settings.canDrawOverlays(context)){
                return true;
            }
            try{
                WindowManager mgr = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                if(mgr == null){
                    return false;
                }

                View viewToAdd = new View(context);

                WindowManager.LayoutParams params = new WindowManager.LayoutParams(0,0, Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O ?
                        WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY : WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSPARENT);

                viewToAdd.setLayoutParams(params);
                mgr.addView(viewToAdd, params);
                mgr.removeView(viewToAdd);
                return true;

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return false;
    }

}
