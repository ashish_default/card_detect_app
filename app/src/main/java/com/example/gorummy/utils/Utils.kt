package com.example.gorummy.utils

import android.app.ActivityManager
import android.content.Context
import android.graphics.Color
import android.widget.Toast
import com.example.gorummy.CoreApplication
import com.muddzdev.styleabletoastlibrary.StyleableToast

class Utils {

    companion object {

        private val context: Context
            get() {
                return CoreApplication.instance
            }

        private val baseToast: StyleableToast.Builder
            get() {
                return StyleableToast.Builder(context)
                        .textColor(Color.WHITE)
                        .backgroundColor(Color.BLACK)
                        .length(Toast.LENGTH_SHORT)
            }

        @JvmStatic
        fun showToast(stringRes: Int) = showToast(context.getString(stringRes))

        @JvmStatic
        fun showToast(msg: String) = baseToast.text(msg).show()

        @JvmStatic
        fun isServiceRunning(serviceClass: Class<*>): Boolean =
                (context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
                        ?.getRunningServices(Int.MAX_VALUE)
                        ?.any { serviceClass.name == it.service.className } == true

    }
}