package com.example.gorummy.utils

import android.view.View
import android.view.ViewTreeObserver

fun View.setVisible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}


fun View.onViewPrepared(callback: (View) -> Unit) {
    val view = this
    this.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            if (view.width == 0 || view.height == 0) {
                return
            }
            view.viewTreeObserver.removeOnGlobalLayoutListener(this)
            callback(view)
        }
    })
}