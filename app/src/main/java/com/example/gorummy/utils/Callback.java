package com.example.gorummy.utils;

public interface Callback<T> {
    boolean onCallback(T result);
}
