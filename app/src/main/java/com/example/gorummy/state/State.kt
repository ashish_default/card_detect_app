package com.example.gorummy.state

import com.example.gorummy.StateManager
import com.example.gorummy.StateName

interface State {
    fun stateName(): StateName

    fun enter(manager: StateManager)

    fun startSelection(manager: StateManager)

}