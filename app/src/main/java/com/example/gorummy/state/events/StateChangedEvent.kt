package com.example.gorummy.state.events

import com.example.gorummy.events.BaseEvent
import com.example.gorummy.state.State

class StateChangedEvent(val state: State): BaseEvent