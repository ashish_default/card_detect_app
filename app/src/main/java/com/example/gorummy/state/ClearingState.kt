package com.example.gorummy.state

import android.util.Log
import com.example.gorummy.StateManager
import com.example.gorummy.StateName

object ClearingState: BaseState() {
    override fun stateName(): StateName = StateName.Clearing
    private val TAG:String = javaClass.simpleName

    override fun enter(manager: StateManager) {
        Log.i(TAG, "enter")
        manager.enterState(InitState)
    }
}