package com.example.gorummy.state

import android.util.Log
import androidx.cardview.widget.CardView
import com.example.gorummy.StateManager
import com.example.gorummy.StateName

object CardDetectResult: BaseState() {
    var manager: StateManager? = null
    override fun stateName(): StateName = StateName.CardResultState
    private val TAG:String = javaClass.simpleName


    override fun enter(manager: StateManager) {
        Log.i(TAG, "inside card detect result state")
        this.manager = manager
        manager.dispatchDrawResultWindow()
        manager?.enterState(ClearingState)
    }
}