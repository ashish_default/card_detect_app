package com.example.gorummy.state

import android.util.Log
import com.example.gorummy.StateManager


abstract class BaseState: State {
    private val tag: String = "BaseState"

    private fun logInfo(msg:String) {
        Log.i(tag, msg)
    }

    override fun startSelection(manager: StateManager) {
        logInfo("startSelection() called")
    }

}