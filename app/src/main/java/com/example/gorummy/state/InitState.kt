package com.example.gorummy.state

import android.util.Log
import com.example.gorummy.StateManager
import com.example.gorummy.StateName

object InitState : BaseState() {
    override fun stateName(): StateName = StateName.Init
    private val TAG:String = InitState.javaClass.simpleName


    override fun enter(manager: StateManager) {
        Log.i(TAG, "enter " + stateName())
    }

    override fun startSelection(manager: StateManager) {
        super.startSelection(manager)
        manager.enterState(ScreenshotTakeState)
    }

}