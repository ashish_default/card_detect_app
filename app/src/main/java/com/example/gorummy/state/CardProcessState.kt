package com.example.gorummy.state

import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.example.gorummy.StateManager
import com.example.gorummy.StateName
import com.example.gorummy.events.EventUtil
import com.example.gorummy.screenshot.events.CardDetectResultEvent
import com.example.gorummy.screenshot.events.ScreenshotTakeEvent
import org.json.JSONObject
import com.example.gorummy.utils.Utils

object CardProcessState: BaseState() {

    var manager: StateManager? = null
    override fun stateName(): StateName = StateName.CardProcessState
    private val TAG:String = javaClass.simpleName


    override fun enter(manager: StateManager) {
        this.manager = manager
        makeApiRequest()
    }

    private fun makeApiRequest(){
        Log.i(TAG, "make api request")

        AndroidNetworking.post("https://98g8cshj1k.execute-api.ap-south-1.amazonaws.com/")
//        AndroidNetworking.get("http://demo0019131.mockable.io/card/detect")
//                .addHeaders("content-type", "application/json")
                .addHeaders("content-type", "image/jpeg")
                .addByteBody(manager?.screenshotByteArray)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {

                    override fun onResponse(response: JSONObject?) {
                        Log.i(TAG, response.toString())
                        EventUtil.post(CardDetectResultEvent())
                        manager?.result = response
                        manager?.enterState(CardDetectResult)
                    }

                    override fun onError(error: ANError) {
                        // handle error
                        Log.i(TAG, error.toString())
                        EventUtil.post(CardDetectResultEvent())
                        manager?.enterState(ClearingState)
                        Utils.showToast(error.toString())
                    }
                })
    }
}