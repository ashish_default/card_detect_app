package com.example.gorummy.state

import android.graphics.Bitmap
import android.util.Log
import com.example.gorummy.StateManager
import com.example.gorummy.StateName
import com.example.gorummy.screenshot.ScreenshotHandler
import com.example.gorummy.utils.ImageFile
import java.io.ByteArrayOutputStream

object ScreenshotTakeState: BaseState() {

    var manager: StateManager ? = null

    override fun stateName(): StateName = StateName.ScreenshotTake

    override fun enter(manager: StateManager) {
        this.manager = manager

        val screenshotHandler = ScreenshotHandler.getInstance()
        if(screenshotHandler.isGetUserPermission) {
            screenshotHandler.setCallback(onScreenshotHandlerCallback)
            screenshotHandler.takeScreenshot(100)
        } else {
            screenshotHandler.getUserPermission()
        }
    }

    private val onScreenshotHandlerCallback = object: ScreenshotHandler.OnScreenshotHandlerCallback {
        override fun onScreenshotStart() {
            manager?.dispatchBeforeScreenshot()
        }

        override fun onScreenshotFinished(screenshotFile: ImageFile?) {
            manager?.apply {
                Log.i(javaClass.simpleName, "on screenshot finished")
                this.screenshotFile = screenshotFile
                dispatchScreenshotSuccess()
                enterState(CardProcessState)
            }
        }

        override fun onScreenshotFailed(errorcode: Int, e: Throwable?) {
            manager?.dispatchScreenshotFailed(errorcode, e)
        }

        override fun onSaveToFile(screenshotByteArray: ByteArray) {
            manager?.screenshotByteArray = screenshotByteArray;
        }
    }

}