package com.example.gorummy;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.gorummy.screenshot.ScreenshotHandler;
import com.example.gorummy.utils.Callback;
import com.example.gorummy.utils.PermissionUtil;
import com.example.gorummy.utils.SettingUtil;

public class MainActivity extends AppCompatActivity {

    protected final String TAG = getClass().getSimpleName();

    public static final String ACTION_START_FROM_NOTIFY = "ACTION_START_FROM_NOTIFY";
    public static final String INTENT_SHOW_FLOATING_VIEW = "INTENT_SHOW_FLOATING_VIEW";

    private final int REQUEST_CODE_CHECK_DRAW_OVERLAY_PERM = 101;
    private final int REQUEST_CODE_EXTERNAL_STORAGE_READ_WRITE = 102;
    private final int REQUEST_CODE_MEDIA_PROJECTION_RESULT = 103;
    private final int REQUEST_CODE_OTP_AUTH = 104;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}
        setContentView(R.layout.activity_main);

        if(getIntent() != null && getIntent().getAction() != null) {
            SettingUtil.INSTANCE.setAppShowing(true);
        }

        Intent authIntent = new Intent(this, PhoneAuthActivity.class);
        startActivityForResult(authIntent, REQUEST_CODE_OTP_AUTH);
//        startApp();

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void startApp(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkDrawOverlayPermission();
        } else {
            requestMediaProjection();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void checkDrawOverlayPermission(){
        Log.i(TAG,"check draw over lay permission");

        if(!PermissionUtil.canDrawOverlays(this)){
            Log.i(TAG, "Requesting draw over lay permission");

            // ask for permission
            final AlertDialog.Builder ab = new AlertDialog.Builder(this);
            ab.setTitle(R.string.need_permission);
            ab.setMessage(R.string.permission_msg);
            ab.setPositiveButton(R.string.btn_setting, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                    try{
                        startActivityForResult(intent, REQUEST_CODE_CHECK_DRAW_OVERLAY_PERM);
//                        ab.setCancelable(true);

                    } catch(ActivityNotFoundException e) {
                        Log.e(TAG, e.getMessage());

                        // alert to show warning and asking for permission again
                        // on negative response close MainActivity
                        AlertDialog.Builder ab1 = new AlertDialog.Builder(MainActivity.this);
                        ab1.setTitle(R.string.error);
                        ab1.setMessage(R.string.error_msg);
                        ab1.setPositiveButton(R.string.button_opnAppPage, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_SETTINGS);
                                startActivityForResult(intent, REQUEST_CODE_CHECK_DRAW_OVERLAY_PERM);
                            }
                        });

                        ab1.setNegativeButton(R.string.btn_close, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                MainActivity.this.finish();
                            }
                        });

                        ab1.show();

                    }
                }
            });

            // close MainActivity
            ab.setNegativeButton(R.string.btn_close, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    MainActivity.this.finish();
                }
            });

            ab.show();

        } else {
            Log.i(TAG, "Has draw over lay permission");
            checkExternalStorageReadWritePermission();
        }

    }

//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void checkExternalStorageReadWritePermission(){
        if(!hasExternalStorageReadWritePermission()){
            Log.i(TAG, "Requesting read write external storage");
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    },
                    REQUEST_CODE_EXTERNAL_STORAGE_READ_WRITE
            );

        } else {
            requestMediaProjection();
        }


    }

    private boolean hasExternalStorageReadWritePermission(){
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void requestMediaProjection(){
        if(ScreenshotHandler.isInitialized()) {
            Log.i(TAG,"Has media projection");
            startService();
        } else {
            Log.i(TAG, "Requesting for media Projection");
            try {
                MediaProjectionManager projectionManager = (MediaProjectionManager) getSystemService(Context.MEDIA_PROJECTION_SERVICE);
                startActivityForResult(projectionManager.createScreenCaptureIntent(), REQUEST_CODE_MEDIA_PROJECTION_RESULT);
            } catch(NoClassDefFoundError e){
                String errorMsg = "[MediaProjection] not found";
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    errorMsg += getString(R.string.error_mediaProjectionNotFound_need5UpperVersion);
                } else {
                    errorMsg += " with unknown situation";
                }
                showErrorDialog(errorMsg, null);
            } catch (Throwable t) {
                String errorMsg = getString(R.string.error_unknownErrorWhenRequestMediaProjection) + " \r\n\r\n" + t.getLocalizedMessage();
                showErrorDialog(errorMsg, null);
            }

        }
    }


    private void showErrorDialog(String msg, final Callback<Void> onRetryCallback){
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle(getString(R.string.error));
        ab.setMessage(msg);
        if(onRetryCallback != null){
            ab.setPositiveButton(getString(R.string.btn_req_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    onRetryCallback.onCallback(null);
                }
            });
        }

        ab.setNegativeButton(R.string.btn_close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                MainActivity.this.finish();
            }
        });

        ab.show();
    }

    private void startService(){
        boolean showFloatingView = true;

        if(getIntent() != null && getIntent().getAction() != null){
            switch (getIntent().getAction()){
                case Intent.ACTION_MAIN:
                    showFloatingView = true;
                    break;
                case ACTION_START_FROM_NOTIFY:
                    if (getIntent().hasExtra(INTENT_SHOW_FLOATING_VIEW)){
                        showFloatingView = getIntent().getBooleanExtra(INTENT_SHOW_FLOATING_VIEW, true);
                    }
                    break;
            }
        }

        ScreenTranslatorService.start(this, showFloatingView);
        finish();
    }

//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CHECK_DRAW_OVERLAY_PERM) {
            onCheckDrawOverlayPermissionResult();
        } else if (requestCode == REQUEST_CODE_MEDIA_PROJECTION_RESULT) {
            onRequestMediaProjectionResult(resultCode, data);
        } else if(requestCode == REQUEST_CODE_OTP_AUTH) {
            startApp();
        }
    }

//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void onCheckDrawOverlayPermissionResult(){
        if(PermissionUtil.canDrawOverlays(this)){
            Log.i(TAG, "Got draw overlay permission");
            checkExternalStorageReadWritePermission();
        } else {
            Log.i(TAG, "User rejected draw overlay permission, show error dialog");
            showErrorDialog(getString(R.string.error_msg), new Callback<Void>() {
                @Override
                public boolean onCallback(Void result) {
                    Log.i(TAG, "Retry to draw overlay permission");
                    checkDrawOverlayPermission();
                    return false;
                }
            });
        }
    }

    private void onRequestMediaProjectionResult(int resultCode, Intent data){
        if(resultCode == RESULT_OK) {
            Log.i(TAG, "Got media projection");
            ScreenshotHandler.init(this).setMediaProjectionIntent(data);
            startService();
        } else {
            Log.i(TAG, "Not got media projection");
            showErrorDialog(getString(R.string.dialog_content_needPermission_mediaProjection), new Callback<Void>() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public boolean onCallback(Void result) {
                    Log.i(TAG, "Retry to get media projection");
                    requestMediaProjection();
                    return false;
                }
            });
        }
    }

//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_EXTERNAL_STORAGE_READ_WRITE) {
            onRequestExternalStorageReadWritePermissionResult();
        }
    }

//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void  onRequestExternalStorageReadWritePermissionResult() {
        if (hasExternalStorageReadWritePermission()) {
            Log.i(TAG,"Got read/write external storage permission");
            requestMediaProjection();
        } else {
            Log.i(TAG,"Not get read/write external storage permission");
            showErrorDialog(getString(R.string.dialog_content_needPermission_rwExternalStorage), new Callback<Void>() {
                @Override
                public boolean onCallback(Void result) {
                    Log.i(TAG,"Retry to get read/write external storage permission");
                    checkExternalStorageReadWritePermission();
                    return false;
                }
            });
        }
    }


    public static void start(Context context) {
        context.startActivity(getStarterIntent(context));
    }

    public static Intent getStarterIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        if (!(context instanceof Activity)) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        return intent;
    }



}
