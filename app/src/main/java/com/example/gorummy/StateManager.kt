package com.example.gorummy

import android.graphics.Bitmap
import android.util.Log
import com.example.gorummy.events.EventUtil
import com.example.gorummy.state.InitState
import com.example.gorummy.state.State
import com.example.gorummy.state.events.StateChangedEvent
import com.example.gorummy.utils.ImageFile
import com.example.gorummy.utils.stateManagerAction
import com.example.gorummy.utils.threadUI
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject

object StateManager {
    var TAG: String = StateManager.javaClass.simpleName;

    var state: State = InitState
    var listener: OnStateChangedListener? = null
    var screenshotFile: ImageFile? = null
    var screenshotByteArray: ByteArray? = null
    var result: JSONObject? = null

    fun enterState(nextState: State) {
        try {
            Log.i(TAG, "State transition: ${state.stateName()} > ${nextState.stateName()}")
            state = nextState
            dispatch {
                listener?.onStateChanged(nextState.stateName())
            }

            EventUtil.postSticky(StateChangedEvent(nextState))
            state.enter(this)
        } catch(err:Exception) {
            Log.e(TAG, err.message)
        }


    }

    fun startSelection() = doAction {
        state.startSelection(this@StateManager)
    }

    private fun doAction(action: () -> Unit) = GlobalScope.launch(stateManagerAction) {
        action()
    }


    private fun dispatch(dispatcher: () -> Unit) =
            threadUI {
                dispatcher()
            }

    // Callback dispatchers

    fun dispatchBeforeScreenshot() = dispatch {
        listener?.beforeScreenshot()
    }

    fun dispatchScreenshotSuccess() = dispatch {
        listener?.screenshotSuccess()
    }

    fun dispatchScreenshotFailed(errorCode: Int, e: Throwable?) = dispatch {
        listener?.screenshotFailed(errorCode, e)
    }

    fun dispatchDrawResultWindow() = dispatch {
        listener?.drawResultWindow()
    }
}

enum class StateName {
    Init, ScreenshotTake, CardProcessState, CardResultState, Clearing
}


interface OnStateChangedListener {
    fun onStateChanged(state: StateName)
    fun beforeScreenshot()
    fun screenshotSuccess()
    fun screenshotFailed(errorCode: Int, e: Throwable?)
    fun drawResultWindow()
}