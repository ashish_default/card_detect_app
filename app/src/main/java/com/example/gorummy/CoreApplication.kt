package com.example.gorummy

import android.app.Application
import com.androidnetworking.AndroidNetworking
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.firebase.FirebaseApp
import okhttp3.OkHttpClient

class CoreApplication : Application() {
    companion object {
        @JvmStatic
        lateinit var instance: CoreApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        initFastAndroidNetworking()

        FirebaseApp.initializeApp(this)
    }

    private fun initFastAndroidNetworking(){
        val okHttpClient = OkHttpClient().newBuilder()
                .addNetworkInterceptor(StethoInterceptor())
                .build()

        AndroidNetworking.initialize(applicationContext, okHttpClient)
    }
}